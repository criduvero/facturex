import { Component, OnInit } from '@angular/core';
import { Festivity } from 'src/app/models/festivities.model';
import { FestivitiesService } from '../festivities.service';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { FestivityModalComponent } from '../festivity-modal/festivity-modal.component';

@Component({
  selector: 'app-festivities-list',
  templateUrl: './festivities-list.component.html',
  styleUrls: ['./festivities-list.component.css'],
  providers: [NgbModalConfig, NgbModal]
})
export class FestivitiesListComponent implements OnInit {

  festivities: Festivity[];
  selectedFestivity: Festivity;
  festivityFilter: any = { name: '' };

  constructor(config: NgbModalConfig, private fService: FestivitiesService, private modalService: NgbModal) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.getFestivities();
    this.fService.festivityUpdated.subscribe((data: any) => {
      this.festivities = this.festivities.map((item: any) => {
        if (item.id === data.id) {
          item = Object.assign({}, item, data);
        }
        return item;
      });
    });
    this.fService.festivityCreated.subscribe((data: any) => {
      this.festivities.push(data);
    });
  }

  getFestivities() {
    this.fService.getFestivities()
      .subscribe(
        (data: Festivity[]) => {
          this.festivities = data;
          // console.log(data);
        },
        error => {
          console.log(error);
          // this.messageService.add({ severity: 'error', summary: 'Error al obtener vehículos', detail: error });
        });
  }

  selectfestivity(festivity) {
    this.selectedFestivity = festivity;
  }

  newModal() {
    const modalRef = this.modalService.open(FestivityModalComponent);
    modalRef.componentInstance.editing = false;
    modalRef.componentInstance.festivities = this.festivities;
  }

  editModal(id) {
    const modalRef = this.modalService.open(FestivityModalComponent);
    modalRef.componentInstance.editing = true;
    modalRef.componentInstance.festivities = this.festivities;
    modalRef.componentInstance.festivityId = id;
  }

}
