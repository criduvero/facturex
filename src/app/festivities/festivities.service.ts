import {throwError as observableThrowError,  Observable } from 'rxjs';

import {catchError, tap} from 'rxjs/operators';
import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Festivity } from '../models/festivities.model';

@Injectable({
  providedIn: 'root'
})
export class FestivitiesService {

  @Output() festivityUpdated: EventEmitter<Festivity> = new EventEmitter<Festivity>();
  @Output() festivityCreated: EventEmitter<Festivity> = new EventEmitter<Festivity>();
  private readonly URL = 'http://localhost:3000/festivities';

  constructor(private http: HttpClient) { }

  getFestivities(): Observable<Festivity[]> {
    return this.http
      .get<Festivity[]>(`${this.URL}`).pipe(
        tap(data => {
          // console.log(data);
        }),
        catchError(this.handleError));
  }

  getFestivity(id): Observable<Festivity> {
    return this.http
      .get<Festivity>(`${this.URL}/${id}`).pipe(
        tap(data => {
          console.log(data);
        }),
        catchError(this.handleError));
  }

  createFestivity(object: Festivity): Observable<Festivity> {
    console.log(object);
    return this.http
      .post<Festivity>(`${this.URL}`, object).pipe(
      tap(data => {
        console.log(data);
        this.festivityCreated.emit(data);
      }),
      catchError(this.handleError));
  }

  updateFestivity(object: Festivity): Observable<Festivity> {
    return this.http
      .put<Festivity>(`${this.URL}/${object.id}`, object).pipe(
      tap(data => {
        console.log(data);
        this.festivityUpdated.emit(data);
      }),
      catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse) {
    console.log(err);
    return observableThrowError(err.error.message);
  }
}
