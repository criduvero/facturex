import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Festivity } from 'src/app/models/festivities.model';
import { FestivitiesService } from '../festivities.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-festivity-modal',
  templateUrl: './festivity-modal.component.html',
  styleUrls: ['./festivity-modal.component.css']
})
export class FestivityModalComponent implements OnInit {

  @Input() editing: boolean;
  @Input() festivities: Festivity[];
  @Input() festivityId: number;
  festivity = new Festivity('', '', null, null);

  constructor(public activeModal: NgbActiveModal,
              private fService: FestivitiesService,
              private toastr: ToastrService) { }

  ngOnInit() {
    console.log(this.festivities);
    // tslint:disable-next-line:no-unused-expression
    this.editing ? this.getFestivity(this.festivityId) : null ;
  }

  getFestivity(id) {
    this.fService.getFestivity(id)
      .subscribe(
        (data: Festivity) => {
          this.festivity = data;
        },
        error => {
          this.toastr.error(`${error}`, 'Error when getting festivity');
        });
  }

  exists() {
    console.log(this.festivities);
    if (this.festivities.length) {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.festivities.length; i++) {
        console.log(this.festivities[i].name);
        if (this.festivity.name.toLowerCase() === this.festivities[i].name.toLowerCase()) {
         return true;
        }
      }
    }
    return false;
  }

  onCreate() {
    if (this.exists()) {
      this.toastr.info('This festivity already exists');
    } else {
      this.fService.createFestivity(this.festivity)
       .subscribe(
         (data: Festivity) => {
           this.toastr.success('The festivity was created successfully');
           this.cerrar(data);
         },
         error => {
           this.toastr.error(`${error}`, 'Festivity creation failed');
         }
       );
    }
   }

   onUpdate() {
    if (this.exists()) {
      this.toastr.info('This festivity already exists');
    } else {
      this.fService.updateFestivity(this.festivity)
       .subscribe(
         (data: Festivity) => {
           this.toastr.success('The festivity was updated successfully');
           this.cerrar(data);
         },
         error => {
          this.toastr.error(`${error}`, 'Festivity update failed');
         }
       );
    }
   }

   onSubmit() {
     this.editing ? this.onUpdate() : this.onCreate();
   }

   cerrar(data) {
    this.activeModal.dismiss('closed');
   }

}
