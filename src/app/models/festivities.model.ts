export class Festivity {
  constructor(
    public name: string,
    public place: string,
    public start: Date,
    public end: Date,
    public id?: number
  ) {  }
}
