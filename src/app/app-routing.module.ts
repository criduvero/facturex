import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FestivitiesListComponent } from './festivities/festivities-list/festivities-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'festivities', pathMatch: 'full'},
  { path: 'festivities', component: FestivitiesListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
